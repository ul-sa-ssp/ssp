# CS5722 Software Architecture Project
## Self-Service Portal - Social Welfare Benefits Management System

Self-Service Portal is an online application portal for citizens of Ireland to apply for the Government’s social welfare schemes. The framework allows for the users to register and create their user account, check from a variety of social welfare schemes rolled out by the City Council (Fuel Allowance, Covid-19 Benefits, Job Seeker’s Benefit, etc.), verify their base eligibility criteria and submit applications for the household in order to receive benefits. Users can enter their applicant, household information, demographics, income and expense-related information to submit an application to receive benefits.

The Self-Service Portal framework enables the City Council to verify the inflow of applications, manage the proofs submitted, determine eligibility for bulk of the applications, calculate benefits based on application information and welfare scheme criteria and issue benefit payments to applicants in the form of debit card, cash and fuel allowance coupons.


The folder **ssp** inside **Code** contains the Source Code.

The folder **Report** contains the latest project documentation, submitted in the form of a report. Has explainer of Architectures, Design Patterns implemented, screenshots of UML diagrams, added value and project critique.

The folder **Diagrams** contains Use Case diagrams, High-Level Architecture diagram, Sequence and Class diagrams.

