package com.direct.dashboard;


import com.ssp.dashboard.service.impl.ActivityHistory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;

import java.util.List;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ActivityHistoryTest {

    private ActivityHistory _history;

    @Before
    public void initialize_ActivityHistoryTest() {
        _history = new ActivityHistory();
        _history.addActivity("activity1");
        _history.addActivity("activity2");
    }

    @After
    public void CleanUp() {
        _history = null;
    }


    @Test
    public void When_invoked_getActivityHistory_then_Should_Get_list_of_activityHistory()    {

        List<String> history = _history.getActivityHistory();
        Assertions.assertEquals(history.size(),2);
        Assertions.assertEquals(history.get(0),"activity1");
        Assertions.assertEquals(history.get(1),"activity2");
    }

    @Test
    public void When_invoked_addActivity_with_valid_input_then_Should_add_activity_to_existinglist()    {

        _history.addActivity("activity3");
        List<String> history = _history.getActivityHistory();
        Assertions.assertEquals(history.size(),3);
        Assertions.assertEquals(history.get(0),"activity1");
        Assertions.assertEquals(history.get(1),"activity2");
        Assertions.assertEquals(history.get(2),"activity3");
    }

    @Test
    public void When_invoked_addActivity_with_invalid_value_then_Should_not_add_activity_to_existinglist()    {

        _history.addActivity("");
        List<String> history = _history.getActivityHistory();
        Assertions.assertEquals(history.size(),2);
        Assertions.assertEquals(history.get(0),"activity1");
        Assertions.assertEquals(history.get(1),"activity2");
    }
}
