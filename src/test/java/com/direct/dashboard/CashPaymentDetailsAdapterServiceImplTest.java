package com.direct.dashboard;

import com.ssp.dashboard.api.common.PaymentInformation;
import com.ssp.dashboard.api.common.PaymentStatus;
import com.ssp.dashboard.service.impl.CashPaymentDetailsAdapterServiceImpl;
import com.ssp.dashboard.service.impl.CashPaymentDetailsServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestInstance;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CashPaymentDetailsAdapterServiceImplTest {

    private CashPaymentDetailsAdapterServiceImpl cashPayment;

    private CashPaymentDetailsServiceImpl cashPaymentDetails;

    @Before
    public void initialize_Test() {

        cashPaymentDetails = new CashPaymentDetailsServiceImpl();
        cashPaymentDetails.NearByPostOffice = "Limerick";
        cashPaymentDetails.PaymentStatus = PaymentStatus.PendingForCollection;
        cashPaymentDetails.BillingAddress = "loran, Limerick,Ireland";
        cashPayment = new CashPaymentDetailsAdapterServiceImpl(cashPaymentDetails);

    }

    @After
    public void CleanUp() {
        cashPayment = null;
    }


    @Test
    public void When_invoked_GetPaymentDetails_then_Should_Get_payment_information_with_valid_status()    {

        PaymentInformation paymentInfo = cashPayment.GetPaymentDetails();
        Assertions.assertEquals(paymentInfo.BillingAddress,"Post Office, Limerick");
        Assertions.assertEquals(paymentInfo.PaymentStatus,"Pending For collection");
        Assertions.assertEquals(paymentInfo.ModeOfPayment,"Cash");
        cashPaymentDetails.PaymentStatus= PaymentStatus.Pending;
        cashPayment =  new CashPaymentDetailsAdapterServiceImpl(cashPaymentDetails);
        paymentInfo = cashPayment.GetPaymentDetails();
        Assertions.assertEquals(paymentInfo.BillingAddress,"Post Office, Limerick");
        Assertions.assertEquals(paymentInfo.PaymentStatus,"Pending Payment ");
        Assertions.assertEquals(paymentInfo.ModeOfPayment,"Cash");

        cashPaymentDetails.PaymentStatus= PaymentStatus.Success;
        cashPayment =  new CashPaymentDetailsAdapterServiceImpl(cashPaymentDetails);
        paymentInfo = cashPayment.GetPaymentDetails();
        Assertions.assertEquals(paymentInfo.BillingAddress,"Post Office, Limerick");
        Assertions.assertEquals(paymentInfo.PaymentStatus,"Amount Collected on 12th April 2020");
        Assertions.assertEquals(paymentInfo.ModeOfPayment,"Cash");

        cashPaymentDetails.PaymentStatus= PaymentStatus.Failed;
        cashPayment =  new CashPaymentDetailsAdapterServiceImpl(cashPaymentDetails);
        paymentInfo = cashPayment.GetPaymentDetails();
        Assertions.assertEquals(paymentInfo.BillingAddress,"Post Office, Limerick");
        Assertions.assertEquals(paymentInfo.PaymentStatus,"Transaction failed");
        Assertions.assertEquals(paymentInfo.ModeOfPayment,"Cash");

    }

}