package com.direct.dashboard;

import com.ssp.dashboard.service.impl.ActivityInterceptorServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestInstance;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ActivityInterceptorServiceImplTest {

    private ActivityInterceptorServiceImpl activityInterceptor ;
//    @Rule
//    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Before
    public void initialize_ActivityHistoryTest() {
        activityInterceptor = new ActivityInterceptorServiceImpl();

    }

    @After
    public void CleanUp() {
        activityInterceptor = null;
    }

    @Test
    public void when_registerService_is_invoked_with_valid_service_parameter_then_should_register_the_service() {
        String activity = "activity";
        activityInterceptor.registerService(activity);
        Assert.assertTrue(activityInterceptor.getActivityList().contains(activity));
    }

    @Test
    public void when_registerService_is_invoked_with_invalid_service_parameter_then_should_not_register_the_service() {
        String activity = "";
        activityInterceptor.registerService(activity);
        Assert.assertTrue(activityInterceptor.getActivityList().isEmpty());
        //Assert.assertFalse(activityInterceptor.getActivityList().contains(activity));
    }

    @Test
    public void when_removeService_is_invoked_with_invalid_service_parameter_then_should_remove_the_service() {
        String activity = "activity1";
        activityInterceptor.registerService(activity);
        Assert.assertTrue(activityInterceptor.getActivityList().contains(activity));
        activityInterceptor.removeService(activity);
        Assert.assertTrue(activityInterceptor.getActivityList().isEmpty());
        Assert.assertFalse(activityInterceptor.getActivityList().contains(activity));
    }

    @Test
    public void when_getActivityList_is_invoked__then_should_remove_then_service() {
        String activity = "activity1";
        activityInterceptor.registerService(activity);
        Assert.assertTrue(activityInterceptor.getActivityList().contains(activity));
        activity = "activity2";
        activityInterceptor.registerService(activity);
        List<String> services = activityInterceptor.getActivityList();
        Assert.assertEquals(services.size(),2);
        Assertions.assertEquals(services.get(0),"activity1");
        Assertions.assertEquals(services.get(1),"activity2");
    }

    @Test
    public void when_dispatchService_is_invoked__then_should_log_the_service() {
        String activity = "activity1";
//        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
//        System.setOut(new PrintStream(outContent));
//        System.setErr(new PrintStream(outContent));
        activityInterceptor.registerService(activity);
        activityInterceptor.dispatchService(activity);
//        Assertions.assertTrue( outContent.toString().contains("The count is : 1 Latest activity :"));
    }
}



