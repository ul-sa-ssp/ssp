package com.direct.dashboard;

import com.ssp.dashboard.api.common.PaymentInformation;
import com.ssp.dashboard.api.common.PaymentStatus;
import com.ssp.dashboard.service.WeightingFactoryService;
import com.ssp.dashboard.service.WeightingService;
import com.ssp.dashboard.service.impl.FuelBenefitsServiceImpl;
import com.ssp.dashboard.service.impl.OnlinePaymentDetailsAdapterServiceImpl;
import com.ssp.dashboard.service.impl.OnlinePaymentDetailsServiceImpl;
import com.ssp.dashboard.service.impl.WeightingFactoryServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FuelBenefitsServiceImplTest {

    private FuelBenefitsServiceImpl fuelBenefits;


    @Before
    public void initialize_Test() {
        WeightingService allowance = new WeightingFactoryServiceImpl().getInstance("limerick");
        fuelBenefits = new FuelBenefitsServiceImpl(allowance);

    }

    @After
    public void CleanUp() {
        fuelBenefits = null;
    }

    @Test
    public void When_invoked_calculateBenefitAmount_of_fuelBenefits_then_Should_return_the_amount() {
        WeightingService allowance = new WeightingFactoryServiceImpl().getInstance("limerick");
        double paymentInfo = fuelBenefits.calculateBenefitAmount(allowance);
        Assertions.assertEquals(paymentInfo, 250.00);
    }

}