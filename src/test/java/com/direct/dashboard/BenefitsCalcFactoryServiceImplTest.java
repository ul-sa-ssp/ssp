package com.direct.dashboard;

import com.ssp.dashboard.service.BenefitsCalcService;
import com.ssp.dashboard.service.impl.BenefitsCalcFactoryServiceImpl;
import com.ssp.dashboard.service.impl.Covid19BenefitsServiceImpl;
import com.ssp.dashboard.service.impl.FuelBenefitsServiceImpl;
import com.ssp.dashboard.service.impl.JobSeekerBenefitServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BenefitsCalcFactoryServiceImplTest {

    private BenefitsCalcService calcServiceInstance;
    private BenefitsCalcFactoryServiceImpl calcFactory;

    @Before
    public void initialize_ActivityHistoryTest() {
        calcFactory = new BenefitsCalcFactoryServiceImpl();

    }

    @After
    public void CleanUp() {
        calcFactory = null;
    }

    @Test
    public void when_getinstance_is_invoked_with_valid_calculationtype_then_should_get_respective_types() {
        String instanceType = "jobseeker";
        calcServiceInstance = calcFactory.getInstance(instanceType);
        Assert.assertTrue(calcServiceInstance instanceof JobSeekerBenefitServiceImpl);

        instanceType = "covid19";
        calcServiceInstance = calcFactory.getInstance(instanceType);
        Assert.assertTrue(calcServiceInstance instanceof Covid19BenefitsServiceImpl);

    }

    @Test
    public void when_getinstance_is_invoked_without_parameter_then_should_get_fuelbenefittype_bydefault() {

        calcServiceInstance = calcFactory.getInstance("");
        Assert.assertTrue(calcServiceInstance instanceof FuelBenefitsServiceImpl);
    }


}
