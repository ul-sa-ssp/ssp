package com.direct.dashboard;

import com.ssp.dashboard.service.WeightingService;
import com.ssp.dashboard.service.impl.FuelBenefitsServiceImpl;
import com.ssp.dashboard.service.impl.JobSeekerBenefitServiceImpl;
import com.ssp.dashboard.service.impl.WeightingFactoryServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class JobSeekerBenefitServiceImplTest {

    private JobSeekerBenefitServiceImpl jobSeekerBenefits;


    @Before
    public void initialize_Test() {
        WeightingService allowance = new WeightingFactoryServiceImpl().getInstance("limerick");
        jobSeekerBenefits = new JobSeekerBenefitServiceImpl(allowance);

    }

    @After
    public void CleanUp() {
        jobSeekerBenefits = null;
    }

    @Test
    public void When_invoked_calculateBenefitAmount_of_jobSeekerBenefits_then_Should_return_the_amount() {
        WeightingService allowance = new WeightingFactoryServiceImpl().getInstance("limerick");
        double paymentInfo = jobSeekerBenefits.calculateBenefitAmount(allowance);
        Assertions.assertEquals(550.0,paymentInfo);
    }
}
