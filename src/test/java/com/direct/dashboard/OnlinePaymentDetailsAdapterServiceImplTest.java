package com.direct.dashboard;

import com.ssp.dashboard.api.common.PaymentInformation;
import com.ssp.dashboard.api.common.PaymentStatus;
import com.ssp.dashboard.service.impl.OnlinePaymentDetailsAdapterServiceImpl;
import com.ssp.dashboard.service.impl.OnlinePaymentDetailsServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.TestInstance;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OnlinePaymentDetailsAdapterServiceImplTest {

    private OnlinePaymentDetailsAdapterServiceImpl paymentAdapter;

    private OnlinePaymentDetailsServiceImpl onlinePaymentDetails;

    @Before
    public void initialize_Test() {

        onlinePaymentDetails = new OnlinePaymentDetailsServiceImpl();
        onlinePaymentDetails.AccountNumber = 123123123;
        onlinePaymentDetails.PaymentStatus = PaymentStatus.Pending;
        onlinePaymentDetails.BillingAddress = "Loran, Limerick, Ireland";
        paymentAdapter = new OnlinePaymentDetailsAdapterServiceImpl(onlinePaymentDetails);

    }

    @After
    public void CleanUp() {
        paymentAdapter = null;
    }

    @Test
    public void When_invoked_GetPaymentDetails_of_onlinepaymentSevice_then_Should_Get_payment_information_with_valid_status()    {

        PaymentInformation paymentInfo = paymentAdapter.GetPaymentDetails();
        Assertions.assertEquals(paymentInfo.BillingAddress,"Loran, Limerick, Ireland");
        Assertions.assertEquals(paymentInfo.PaymentStatus,"Pending Payment ");
        Assertions.assertEquals(paymentInfo.ModeOfPayment,"Online Account transfer");
        onlinePaymentDetails.PaymentStatus= PaymentStatus.Pending;
        paymentAdapter =  new OnlinePaymentDetailsAdapterServiceImpl(onlinePaymentDetails);
        paymentInfo = paymentAdapter.GetPaymentDetails();
        Assertions.assertEquals(paymentInfo.BillingAddress,"Loran, Limerick, Ireland");
        Assertions.assertEquals(paymentInfo.PaymentStatus,"Pending Payment ");
        Assertions.assertEquals(paymentInfo.ModeOfPayment,"Online Account transfer");

        onlinePaymentDetails.PaymentStatus= PaymentStatus.Success;
        paymentAdapter =  new OnlinePaymentDetailsAdapterServiceImpl(onlinePaymentDetails);
        paymentInfo = paymentAdapter.GetPaymentDetails();
        Assertions.assertEquals(paymentInfo.BillingAddress,"Loran, Limerick, Ireland");
        Assertions.assertEquals(paymentInfo.PaymentStatus,"Amount transfered on 12th April 2020");
        Assertions.assertEquals(paymentInfo.ModeOfPayment,"Online Account transfer");

        onlinePaymentDetails.PaymentStatus= PaymentStatus.Failed;
        paymentAdapter =  new OnlinePaymentDetailsAdapterServiceImpl(onlinePaymentDetails);
        paymentInfo = paymentAdapter.GetPaymentDetails();
        Assertions.assertEquals(paymentInfo.BillingAddress,"Loran, Limerick, Ireland");
        Assertions.assertEquals(paymentInfo.PaymentStatus,"Transaction failed ");
        Assertions.assertEquals(paymentInfo.ModeOfPayment,"Online Account transfer");

    }
}
