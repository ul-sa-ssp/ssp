package com.ssp.dashboard.service;

import com.ssp.dashboard.api.common.Message;
import com.ssp.dashboard.service.impl.UserInformationServiceImpl;

public interface INotificationStrategyService {

    void NotifyUser(UserInformationServiceImpl userInformationServiceImpl, Message messageServiceImpl);
}
