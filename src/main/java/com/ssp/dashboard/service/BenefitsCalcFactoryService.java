package com.ssp.dashboard.service;

public interface BenefitsCalcFactoryService {

    public BenefitsCalcService getInstance(String type);
}
