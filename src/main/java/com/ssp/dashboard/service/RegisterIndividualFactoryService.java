package com.ssp.dashboard.service;

public interface RegisterIndividualFactoryService {

    public RegisterIndividualService getInstance(String type);
}
