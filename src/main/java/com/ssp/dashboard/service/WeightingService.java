package com.ssp.dashboard.service;

public interface WeightingService {
    public double calculateLocationAllowance();
}
