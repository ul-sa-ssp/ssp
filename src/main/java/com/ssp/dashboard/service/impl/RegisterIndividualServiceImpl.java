package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.db.dao.RegistrarIndividualDao;
import com.ssp.dashboard.api.db.domain.RegistrarIndividual;

import com.ssp.dashboard.api.ui.RegisterIndividualUI;
import com.ssp.dashboard.service.RegisterIndividualService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class RegisterIndividualServiceImpl implements RegisterIndividualService {

    @Override
    public void registrarIndividual(RegisterIndividualUI registerIndividualUI, Integer id) {
        RegistrarIndividual registrarIndividual = new RegistrarIndividual();
        RegistrarIndividualDao registrarIndividualDao = ApplicationContextHolder.getContext().getBean(RegistrarIndividualDao.class);
        registrarIndividual.setAppId(id);
        registrarIndividual.setFirstName(registerIndividualUI.getFirstName());
        registrarIndividual.setLastName(registerIndividualUI.getLastName());
        registrarIndividual.setBirthDate(registerIndividualUI.getBirthDate());
        registrarIndividual.setMaritalStatus(registerIndividualUI.getMaritalStatus());
        registrarIndividual.setEducationLevel(registerIndividualUI.getEducationLevel());
        registrarIndividual.setRelationshipToApplication(registerIndividualUI.getRelationshipToApplication());
        registrarIndividual.setEarningMember(registerIndividualUI.getEarningMember());
        registrarIndividual.setHouseholdStatus(registerIndividualUI.getHouseholdStatus());
        System.out.println(registrarIndividual.getFirstName());
        registrarIndividualDao.save(registrarIndividual);
    }
}
