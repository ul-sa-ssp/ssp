package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.CommandService;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

//This is the invoker that's actually executing commands.
//Starts a worker thread in charge of executing commands

@Service
public class EDTaskRunnerImpl implements Runnable{
    private Thread runner;
    private List<CommandService> pendingCommands;
    private volatile boolean stop;

    private static final EDTaskRunnerImpl RUNNER = new EDTaskRunnerImpl();

    public static final EDTaskRunnerImpl getInstance(){
        return RUNNER;
    }

    private EDTaskRunnerImpl(){
        pendingCommands = new LinkedList<>();
        runner = new Thread(this);
        runner.start();
    }

    //Run method takes pending commands and executes them.
    @Override
    public void run() {

        while (true) {
            CommandService cmd = null;
            synchronized (pendingCommands) {
                System.out.println("Hi");
                if (pendingCommands.isEmpty()) {
                    try {
                        pendingCommands.wait();
                    } catch (InterruptedException e) {
                        System.out.println("Runner interrupted");
                        if (stop) {
                            System.out.println("Runner stopping");
                            return;
                        }
                    }
                } else {
                    cmd = pendingCommands.remove(0);
                }
            }
            if (cmd == null){
                System.out.println(cmd);
                return;
            }
            cmd.execute();
        }

    }

    //Scheduling commands for later execution
    public void addCommand(CommandService cmd) {
        synchronized (pendingCommands) {
            pendingCommands.add(cmd);
            pendingCommands.notifyAll();
        }
    }

    public void shutdown() {
        this.stop = true;
        this.runner.interrupt();
    }

}
