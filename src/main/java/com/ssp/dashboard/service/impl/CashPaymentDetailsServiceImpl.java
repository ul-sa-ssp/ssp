package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.common.PaymentStatus;
import org.springframework.stereotype.Service;

@Service
public class CashPaymentDetailsServiceImpl {

    public com.ssp.dashboard.api.common.PaymentStatus PaymentStatus;
    public String NearByPostOffice;
    public String BillingAddress;
}
