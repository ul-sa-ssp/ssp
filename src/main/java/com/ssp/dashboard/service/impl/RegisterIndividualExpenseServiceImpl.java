package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.db.dao.RegistrarIndividualExpenseDao;
import com.ssp.dashboard.api.db.domain.RegistrarIndividualExpense;
import com.ssp.dashboard.api.ui.RegisterIndividualUI;
import com.ssp.dashboard.service.RegisterIndividualService;
import org.springframework.stereotype.Service;

@Service
public class RegisterIndividualExpenseServiceImpl implements RegisterIndividualService {

    @Override
    public void registrarIndividual(RegisterIndividualUI registerIndividualUI, Integer id) {
        RegistrarIndividualExpense registrarIndividualExpense = new RegistrarIndividualExpense();
        RegistrarIndividualExpenseDao registrarIndividualExpenseDao = ApplicationContextHolder.getContext().getBean(RegistrarIndividualExpenseDao.class);
        registrarIndividualExpense.setIndvId(id);
        registrarIndividualExpense.setExpenseType(registerIndividualUI.getExpenseType());
        registrarIndividualExpense.setExpenseFrequency(registerIndividualUI.getExpenseFrequency());
        registrarIndividualExpense.setAmount(registerIndividualUI.getExpenseAmount());
        registrarIndividualExpenseDao.save(registrarIndividualExpense);
    }
}
