package com.ssp.dashboard.service.impl;

import java.util.ArrayList;
import java.util.List;

public class ActivityHistory {
    private static List<String> activityHistory;

    public ActivityHistory () {
        this.activityHistory = new ArrayList<>();;
    }

    public List<String> getActivityHistory() {
        return activityHistory;
    }

    public void addActivity(String s) {
        if(s.isEmpty())
        {
            return;
        }
        activityHistory.add(s);
    }
}
