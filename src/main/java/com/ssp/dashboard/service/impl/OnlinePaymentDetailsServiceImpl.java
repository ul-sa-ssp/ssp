package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.common.PaymentStatus;
import org.springframework.stereotype.Service;

@Service
public class OnlinePaymentDetailsServiceImpl {

    public com.ssp.dashboard.api.common.PaymentStatus PaymentStatus;
    public int AccountNumber;
    public String BillingAddress;
    public String PaymentDate;
    public String transactionId;
}
