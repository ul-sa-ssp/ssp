package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.CommandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

public class DetermineEligibilityCommandServiceImpl implements CommandService{

    private List<String> applicationIDs;
    private String program;
    private EDServiceImpl receiver;

    public DetermineEligibilityCommandServiceImpl(List<String> appIDs, String program, EDServiceImpl service){
        this.applicationIDs = appIDs;
        this.program = program;
        this.receiver = service;
    }

    @Override
    public void execute() {

        System.out.println("Execute");
        receiver.determineEligibility(applicationIDs, program);
    }
}
