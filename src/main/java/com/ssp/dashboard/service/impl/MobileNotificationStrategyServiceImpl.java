package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.common.Message;
import com.ssp.dashboard.service.INotificationStrategyService;
import org.springframework.stereotype.Service;

@Service
public class MobileNotificationStrategyServiceImpl implements INotificationStrategyService {

    int _recipient;

    // Sender's email ID needs to be mentioned
    int sender = 567567;

    boolean ValidateInformation(int recipient, Message messageServiceImpl)
    {
        if(recipient == 0 || messageServiceImpl == null || messageServiceImpl.Content.isEmpty() || messageServiceImpl.subject.isEmpty())
            return false;

        _recipient = recipient;
        return true;
    }

    void SendSMS(UserInformationServiceImpl userInformationServiceImpl, Message messageServiceImpl)
    {
        if (ValidateInformation(userInformationServiceImpl.MobileNumber, messageServiceImpl)) {
            //connect to the smtp server and send the mail
            System.out.println("SMS sent successfully");
        }
    }
    @Override
    public void NotifyUser(UserInformationServiceImpl userInformationServiceImpl, Message messageServiceImpl) {

        SendSMS (userInformationServiceImpl, messageServiceImpl);
    }
}
