package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.WeightingService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class LimerickWeightingServiceImpl implements WeightingService {

    @Override
    public double calculateLocationAllowance() {
        return 50;
    }
}
