package com.ssp.dashboard.service.impl;


import com.ssp.dashboard.api.db.dao.PaymentDAO;
import com.ssp.dashboard.api.db.dao.VisitableEntity;
import com.ssp.dashboard.api.db.domain.MetricsEntity;
import com.ssp.dashboard.api.db.domain.PaymentsEntity;
import com.ssp.dashboard.service.MetricsVisitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class MetricsVisitorServiceImpl implements MetricsVisitorService {

    private PaymentDAO paymentDAO;

    private VisitableEntity visitableEntity;

    private List<MetricsEntity> metricsEntityList = new ArrayList<>();

    @Autowired
    public MetricsVisitorServiceImpl(PaymentDAO paymentDAO) {
        this.paymentDAO = paymentDAO;
    }

    @Override
    @Transactional
    public List<MetricsEntity> findAll() {

        List<String> schemeName = new ArrayList<>();
        schemeName.add("COVID19");
        schemeName.add("FUEL");

        for (String s : schemeName) {
            List<PaymentsEntity> schemePayments = this.paymentDAO.getDetails(s);
            MetricsEntity metricsEntity = new MetricsEntity(s, "Cost to Goverment", 0,0);
            for (PaymentsEntity p : schemePayments) {
                metricsEntity.setCountOfTotalApplicants(metricsEntity.getCountOfTotalApplicants() + 1);
                metricsEntity.setTotalAmountPaid(metricsEntity.getTotalAmountPaid() + p.getAmount());
            }

            metricsEntity.accept(this);

        }

        return this.metricsEntityList;
    }

    @Transactional
    public List<PaymentsEntity> findbyScheme(String s) {
        return this.paymentDAO.getDetails(s);
    }

    @Override
    public void updateDetailsByEntity(MetricsEntity metricsEntity) {
        this.metricsEntityList.add(metricsEntity);
    }
}
