package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.db.dao.RegistrarIndividualIncomeDao;
import com.ssp.dashboard.api.db.domain.RegistrarIndividualIncome;
import com.ssp.dashboard.api.ui.RegisterIndividualUI;
import com.ssp.dashboard.service.RegisterIndividualService;
import org.springframework.stereotype.Service;

@Service
public class RegisterIndividualIncomeServiceImpl implements RegisterIndividualService {

    @Override
    public void registrarIndividual(RegisterIndividualUI registerIndividualUI, Integer id) {
        RegistrarIndividualIncome registrarIndividualIncome = new RegistrarIndividualIncome();
        RegistrarIndividualIncomeDao registrarIndividualIncomeDao = ApplicationContextHolder.getContext().getBean(RegistrarIndividualIncomeDao.class);
        registrarIndividualIncome.setIncomeType(registerIndividualUI.getIncomeType());
        registrarIndividualIncome.setIncomeFrequency(registerIndividualUI.getIncomeFrequency());
        registrarIndividualIncome.setAmount(registerIndividualUI.getIncomeAmount());
        registrarIndividualIncome.setIndvId(id);
        registrarIndividualIncomeDao.save(registrarIndividualIncome);
    }
}
