package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.RegisterIndividualFactoryService;
import com.ssp.dashboard.service.RegisterIndividualService;
import org.springframework.stereotype.Service;

@Service
public class RegisterIndividualFactoryServiceImpl implements RegisterIndividualFactoryService {
    @Override
    public RegisterIndividualService getInstance(String type) {
            if(type.equals("individual")){
                return new RegisterIndividualServiceImpl();
            }
            else if(type.equals("income")){
                return new RegisterIndividualIncomeServiceImpl();
            }
            else{
                return new RegisterIndividualExpenseServiceImpl();
            }
    }
}
