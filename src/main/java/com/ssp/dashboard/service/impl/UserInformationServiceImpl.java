package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.common.UserNotificationPreference;
import org.springframework.stereotype.Service;


public class UserInformationServiceImpl {

    String FirstName;
    String LastName ;
    UserNotificationPreference UserPreference = null;
    int MobileNumber;
    String Address ;
    String Email ;

    public UserInformationServiceImpl(String firstname, String lastName, String email,
                                      String address, int mobileNumber, UserNotificationPreference userPreference)
    {
        FirstName = firstname;
        LastName = lastName;
        Address= address;
        Email = email;
        MobileNumber = mobileNumber;
        UserPreference = userPreference;
    }
}
