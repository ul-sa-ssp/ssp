package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.db.dao.RegistrarIndividualDao;
import com.ssp.dashboard.api.db.dao.RegistrarIndividualExpenseDao;
import com.ssp.dashboard.api.db.dao.RegistrarIndividualIncomeDao;
import com.ssp.dashboard.api.db.dao.RegistrationDao;
import com.ssp.dashboard.api.db.domain.RegistrarIndividual;
import com.ssp.dashboard.api.db.domain.RegistrarIndividualExpense;
import com.ssp.dashboard.api.db.domain.RegistrarIndividualIncome;
import com.ssp.dashboard.api.db.domain.Registration;
import com.ssp.dashboard.service.LookupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class LookupServiceImpl implements LookupService {

    @Autowired
    private RegistrationDao registrationDao;

    @Autowired
    private RegistrarIndividualIncomeDao registrarIndividualIncomeDao;

    @Autowired
    private RegistrarIndividualExpenseDao registrarIndividualExpenseDao;

    @Autowired
    private RegistrarIndividualDao registrarIndividualDao;

    @Override
    public List<Registration> getRegistrar() {
        return registrationDao.findAll();
    }

    @Override
    public List<RegistrarIndividual> getRegistrarIndividual() {
        return registrarIndividualDao.findAll();
    }

    @Override
    public List<RegistrarIndividualExpense> getRegistrarIndividualExpense() {
        return registrarIndividualExpenseDao.findAll();
    }

    @Override
    public List<RegistrarIndividualIncome> getRegistrarIndividualIncome() {
        return registrarIndividualIncomeDao.findAll();
    }
    
}
