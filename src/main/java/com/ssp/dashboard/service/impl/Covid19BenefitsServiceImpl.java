package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.BenefitsCalcService;
import com.ssp.dashboard.service.WeightingService;
import org.springframework.stereotype.Service;

@Service
public class Covid19BenefitsServiceImpl implements BenefitsCalcService {

    protected WeightingService weighting;
    private double grossHouseholdIncome = 1000;

    public Covid19BenefitsServiceImpl(WeightingService weighting){
        this.weighting = weighting;
    }

    @Override
    public double calculateBenefitAmount(WeightingService weighting) {
        double benefitAmount = grossHouseholdIncome / 10;
        return benefitAmount + weighting.calculateLocationAllowance();
    }
}
