package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.common.PaymentInformation;
import com.ssp.dashboard.api.common.PaymentStatus;
import com.ssp.dashboard.service.PaymentDetailsService;
import org.springframework.stereotype.Service;

@Service
public class OnlinePaymentDetailsAdapterServiceImpl implements PaymentDetailsService {

    private OnlinePaymentDetailsServiceImpl paymentDetailsadaptee;

    public OnlinePaymentDetailsAdapterServiceImpl(OnlinePaymentDetailsServiceImpl adaptee) {
        this.paymentDetailsadaptee = adaptee;
    }
    @Override
    public PaymentInformation GetPaymentDetails() {
        PaymentInformation details = new PaymentInformation();
        details.ModeOfPayment = "Online Account transfer";
        details.PaymentStatus = paymentDetailsadaptee.PaymentStatus == PaymentStatus.Success ? "Amount transfered on 12th April 2020"
                : (paymentDetailsadaptee.PaymentStatus == PaymentStatus.Pending ? "Pending Payment " : "Transaction failed " );
        details.BillingAddress = "Loran, Limerick, Ireland";
        return details;
    }
}
