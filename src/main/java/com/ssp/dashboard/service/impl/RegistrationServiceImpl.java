package com.ssp.dashboard.service.impl;


import com.ssp.dashboard.api.db.dao.RegistrationDao;

import com.ssp.dashboard.api.db.domain.Registration;

import com.ssp.dashboard.api.ui.RegistrationUI;
import com.ssp.dashboard.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Autowired
    private RegistrationDao registrationDao;

    @Override
    public void registrationUser(RegistrationUI registrationUI) {
        Registration registration = new Registration();
        registration.setFirstName(registrationUI.getFirstName());
        registration.setLastName(registrationUI.getLastName());
        registration.setEmail(registrationUI.getEmail());
        registration.setPhoneNumber(registrationUI.getPhone());
        registration.setBirthDate(registrationUI.getBirthDate());
        registrationDao.save(registration);
    }
}
