package com.ssp.dashboard.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;

//This class is the receiver.
@Service
public class EDServiceImpl{

    // determine eligibility for application to given program
    public void determineEligibility(List<String> appIDs, String program){

        System.out.println("ED run for " +program+ " starts...");
        System.out.println("Eligible Applications for " +program+ " are:");

        /*     TODO: logic to pick out eligible apps after ED run, to be added
         */

        if (appIDs != null){
            for(String appID: appIDs){
                System.out.println(appID);
            }
        }

        System.out.println("ED run for " +program+ " completed");
    }

}
