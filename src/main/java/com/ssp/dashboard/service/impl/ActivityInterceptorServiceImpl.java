package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.ActivityInterceptorService;
import com.ssp.dashboard.service.DispatcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ActivityInterceptorServiceImpl implements ActivityInterceptorService {

    private List<String> service;
    private DispatcherService dispatcherService;
    static ActivityHistory activityHistory;

    @Autowired
    public ActivityInterceptorServiceImpl() {
        service = new ArrayList<>();
        this.dispatcherService = new DispatcherServiceImpl();
        activityHistory = new ActivityHistory();
    }

    @Override
    public void registerService(String object) {
        activityHistory.addActivity(object);
        service = activityHistory.getActivityHistory();
    }

    @Override
    public void removeService(String object) {
        // Logic to remove activity if needed
        activityHistory.getActivityHistory().remove(object);
    }

    @Override
    public List<String> getActivityList() {
        return activityHistory.getActivityHistory();
    }

    @Override
    public void dispatchService(String object) {
        dispatcherService.reportActivity(service);
    }
}
