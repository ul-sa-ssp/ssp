package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.BenefitsCalcFactoryService;
import com.ssp.dashboard.service.BenefitsCalcService;
import com.ssp.dashboard.service.WeightingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BenefitsCalcFactoryServiceImpl implements BenefitsCalcFactoryService {

    @Autowired
    private WeightingService weighting;

    @Override
    public BenefitsCalcService getInstance(String type) {

            if(type.equals("jobseeker")){
                return new JobSeekerBenefitServiceImpl(weighting);
            }
            else if(type.equals("covid19")){
                return new Covid19BenefitsServiceImpl(weighting);
            }
            else{
                return new FuelBenefitsServiceImpl(weighting);
            }
    }
}
