package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.BenefitsCalcService;
import com.ssp.dashboard.service.WeightingService;
import org.springframework.stereotype.Service;

@Service
public class FuelBenefitsServiceImpl implements BenefitsCalcService {

    protected WeightingService weighting;
    private double grossHouseholdIncome = 1000;

    public FuelBenefitsServiceImpl(WeightingService weighting){
        this.weighting = weighting;
    }

    @Override
    public double calculateBenefitAmount(WeightingService weighting) {
        double benefitAmount = grossHouseholdIncome / 5;
        return benefitAmount + weighting.calculateLocationAllowance();
    }
}
