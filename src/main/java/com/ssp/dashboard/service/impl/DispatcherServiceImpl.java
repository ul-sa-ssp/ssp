package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.DispatcherService;

import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DispatcherServiceImpl implements DispatcherService {

    private Logger logger;

    public DispatcherServiceImpl() {
        this.logger = Logger.getLogger(this.getClass().getName());
        ConsoleHandler consoleHandler = new ConsoleHandler();
        logger.addHandler(consoleHandler);
    }

    @Override
    public void reportActivity(List<String> str) {

        logger.log(Level.INFO, "The count is : " + str.size() + " Latest activity : " + str.get(str.size() - 1));

    }
}
