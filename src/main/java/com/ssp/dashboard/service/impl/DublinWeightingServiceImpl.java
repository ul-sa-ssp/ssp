package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.WeightingService;
import org.springframework.stereotype.Service;

@Service
public class DublinWeightingServiceImpl implements WeightingService {

    @Override
    public double calculateLocationAllowance() {
        return 150;
    }
}
