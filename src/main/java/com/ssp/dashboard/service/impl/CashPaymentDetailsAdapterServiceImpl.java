package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.common.PaymentInformation;
import com.ssp.dashboard.api.common.PaymentStatus;
import com.ssp.dashboard.service.PaymentDetailsService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class CashPaymentDetailsAdapterServiceImpl implements PaymentDetailsService {

    private CashPaymentDetailsServiceImpl paymentDetailsadaptee;

    public CashPaymentDetailsAdapterServiceImpl(CashPaymentDetailsServiceImpl adaptee) {
        this.paymentDetailsadaptee = adaptee;
    }

    @Override
    public PaymentInformation GetPaymentDetails() {
        PaymentInformation details = new PaymentInformation();
        details.ModeOfPayment = "Cash";

        details.PaymentStatus = paymentDetailsadaptee.PaymentStatus == PaymentStatus.Success ? "Amount Collected on 12th April 2020"
                : (paymentDetailsadaptee.PaymentStatus == PaymentStatus.Pending ? "Pending Payment " :
                (paymentDetailsadaptee.PaymentStatus == PaymentStatus.PendingForCollection ? "Pending For collection" :"Transaction failed" ));

        details.BillingAddress = "Post Office, Limerick";
        return details;
    }
}
