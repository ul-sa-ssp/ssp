package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.BenefitsCalcFactoryService;
import com.ssp.dashboard.service.BenefitsCalcService;
import com.ssp.dashboard.service.WeightingFactoryService;
import com.ssp.dashboard.service.WeightingService;
import org.springframework.stereotype.Service;

@Service
public class WeightingFactoryServiceImpl implements WeightingFactoryService {

    protected WeightingService weighting;
    double grossHouseholdIncome = 500;

    @Override
    public WeightingService getInstance(String type) {

            if(type.equals("limerick")){
                return new LimerickWeightingServiceImpl();
            }
            else{
                return new DublinWeightingServiceImpl();
            }
    }
}
