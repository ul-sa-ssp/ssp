package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.common.Message;
import com.ssp.dashboard.service.INotificationStrategyService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class EmailNotificationStrategyServiceImpl implements INotificationStrategyService {
    String _mailRecipient = "";

    // Sender's email ID needs to be mentioned
    String sender = "government.ie@gmail.com";

    boolean ValidateInformation(String recipient, Message MessageServiceImpl)
    {
        if(recipient.isEmpty() || MessageServiceImpl == null || MessageServiceImpl.Content.isEmpty() || MessageServiceImpl.subject.isEmpty())
            return false;

        _mailRecipient = recipient;
        return true;
    }

    void SendEmail(UserInformationServiceImpl userInformationServiceImpl, Message messageServiceImpl)
    {
        if (ValidateInformation(userInformationServiceImpl.Email, messageServiceImpl)) {
            //connect to the smtp server and send the mail
            System.out.println("email sent successfully");
        }
    }
    @Override
    public void NotifyUser(UserInformationServiceImpl userInformationServiceImpl, Message messageServiceImpl) {

        SendEmail (userInformationServiceImpl, messageServiceImpl);
    }
}
