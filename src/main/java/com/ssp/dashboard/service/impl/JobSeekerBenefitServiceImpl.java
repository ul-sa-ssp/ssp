package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.service.BenefitsCalcService;
import com.ssp.dashboard.service.WeightingService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class JobSeekerBenefitServiceImpl implements BenefitsCalcService {

    protected WeightingService weighting;
    private double grossHouseholdIncome = 1000;

    public JobSeekerBenefitServiceImpl(WeightingService weighting){
        this.weighting = weighting;
    }

    @Override
    public double calculateBenefitAmount(WeightingService weighting) {
        double benefitAmount = grossHouseholdIncome / 2;
        return benefitAmount + weighting.calculateLocationAllowance();
    }
}
