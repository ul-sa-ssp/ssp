package com.ssp.dashboard.service.impl;

import com.ssp.dashboard.api.common.Message;
import com.ssp.dashboard.api.common.UserNotificationPreference;
import com.ssp.dashboard.service.INotificationStrategyService;
import com.ssp.dashboard.service.SendNotificationService;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ssp.dashboard.api.common.UserNotificationPreference.*;

@Service
public class SendNotificationServiceImpl implements SendNotificationService {

private INotificationStrategyService _notificationStrategy;

    @Override
    public void SendNotify() {
        UserInformationServiceImpl[] UserCollection = new UserInformationServiceImpl[]{
                new UserInformationServiceImpl("Jansi", "Subburam", "jansi@gmail.com", "Limerick,Ireland", 123123123, Email),
                new UserInformationServiceImpl("Tom", "Sawyer", "tom@gmail.com", "Limerick,Ireland", 123123123, Mobile),
        };


        Message message = new Message();
        message.Content = "Covid- 19 umemployment scheme description";
        message.subject = "Covid- 19 umemployment scheme!!";

        for (UserInformationServiceImpl details : UserCollection) {

            switch (details.UserPreference) {
                case Email:
                    _notificationStrategy = new EmailNotificationStrategyServiceImpl();
                    break;
                case Mobile:
                default:
                    _notificationStrategy = new MobileNotificationStrategyServiceImpl();
                    break;
            }
            _notificationStrategy.NotifyUser(details,message);

        }
    }
}
