package com.ssp.dashboard.service;

public interface BenefitsCalcService {
    public  double calculateBenefitAmount(WeightingService weightingService);
}

