package com.ssp.dashboard.service;

public interface WeightingFactoryService {

    public WeightingService getInstance(String type);
}
