package com.ssp.dashboard.service;

public interface CommandService {

    void execute();
}
