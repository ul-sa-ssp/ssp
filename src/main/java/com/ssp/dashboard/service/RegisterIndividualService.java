package com.ssp.dashboard.service;


import com.ssp.dashboard.api.ui.RegisterIndividualUI;

public interface RegisterIndividualService {

    public void registrarIndividual(RegisterIndividualUI registerIndividualUI, Integer id);
}
