package com.ssp.dashboard.service;

import java.util.List;

public interface ActivityInterceptorService {

    public void registerService(String str);

    public void removeService(String str);

    public void dispatchService(String str);

    public List<String> getActivityList();

}
