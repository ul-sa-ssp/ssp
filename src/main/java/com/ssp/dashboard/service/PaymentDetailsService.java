package com.ssp.dashboard.service;

import com.ssp.dashboard.api.common.PaymentInformation;

public interface PaymentDetailsService {

    public PaymentInformation GetPaymentDetails()   ;
}
