package com.ssp.dashboard.service;



import com.ssp.dashboard.api.db.domain.MetricsEntity;

import java.util.List;

public interface MetricsVisitorService {
    public List<MetricsEntity> findAll();

    public void updateDetailsByEntity(MetricsEntity metricsEntity);
}
