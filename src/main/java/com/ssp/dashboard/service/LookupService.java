package com.ssp.dashboard.service;

import com.ssp.dashboard.api.db.domain.RegistrarIndividual;
import com.ssp.dashboard.api.db.domain.RegistrarIndividualExpense;
import com.ssp.dashboard.api.db.domain.RegistrarIndividualIncome;
import com.ssp.dashboard.api.db.domain.Registration;

import java.util.List;

public interface LookupService {

    public List<Registration> getRegistrar();

    public List<RegistrarIndividual> getRegistrarIndividual();

    public List<RegistrarIndividualExpense> getRegistrarIndividualExpense();

    public List<RegistrarIndividualIncome> getRegistrarIndividualIncome();
}
