package com.ssp.dashboard;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SSPDashboardApplication {

    public static void main( String[] args )
    {
        SpringApplication.run( SSPDashboardApplication.class, args );
    }
}
