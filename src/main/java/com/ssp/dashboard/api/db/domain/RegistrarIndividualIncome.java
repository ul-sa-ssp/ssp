package com.ssp.dashboard.api.db.domain;

import javax.persistence.*;

@Entity
@Table(name = "dc_income")
public class RegistrarIndividualIncome {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "indv_income_id")
    private Integer indvIncomeId;

    @Column(name = "indv_id")
    private Integer indvId;

    @Column(name = "income_type")
    private String incomeType;

    @Column(name = "income_frequency")
    private String incomeFrequency;

    @Column(name = "amount")
    private Double amount;


    public String getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(String incomeType) {
        this.incomeType = incomeType;
    }

    public String getIncomeFrequency() {
        return incomeFrequency;
    }

    public void setIncomeFrequency(String incomeFrequency) {
        this.incomeFrequency = incomeFrequency;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getIndvId() {
        return indvId;
    }

    public void setIndvId(Integer indvId) {
        this.indvId = indvId;
    }
}
