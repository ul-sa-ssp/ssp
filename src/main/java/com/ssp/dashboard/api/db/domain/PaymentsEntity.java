package com.ssp.dashboard.api.db.domain;

import javax.persistence.*;
import java.sql.Time;


@Entity
@Table(name = "dc_payouts")
public class PaymentsEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "payment_id")
    private int paymentId;

    @Column(name = "scheme_name")
    private String schemeName;

    @Column(name = "payment_timestamp")
    private Time paymentTimestamp;

    @Column(name = "amount")
    private float amount;

    @Column(name = "indv_id")
    private int indvId;

    public PaymentsEntity() {

    }

    public PaymentsEntity(String schemeName, Time time, Float amount, int indvId) {
        this.schemeName = schemeName;
        this.amount = amount;
        this.paymentTimestamp = time;
        this.indvId = indvId;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public Time getPaymentTimestamp() {
        return paymentTimestamp;
    }

    public void setPaymentTimestamp(Time paymentTimestamp) {
        this.paymentTimestamp = paymentTimestamp;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public int getIndvId() {
        return indvId;
    }

    public void setIndvId(int indvId) {
        this.indvId = indvId;
    }

    @Override
    public String toString() {
        return "PaymentsEntity{" +
                "paymentId=" + paymentId +
                ", schemeName=" + schemeName +
                ", paymentTimestamp=" + paymentTimestamp +
                ", amount=" + amount +
                ", indvId=" + indvId +
                '}';
    }
}
