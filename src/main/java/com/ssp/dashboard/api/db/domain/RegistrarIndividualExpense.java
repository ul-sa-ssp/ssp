package com.ssp.dashboard.api.db.domain;

import javax.persistence.*;


    @Entity
    @Table(name = "dc_expense")
    public class RegistrarIndividualExpense {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "indv_expense_id")
        private Integer indvExpenseId;

        @Column(name = "indv_id")
        private Integer indvId;

        @Column(name = "expense_type")
        private String expenseType;

        @Column(name = "expense_frequency")
        private String expenseFrequency;

        @Column(name = "amount")
        private Double amount;

        public Integer getIndvExpenseId() {
            return indvExpenseId;
        }

        public void setIndvExpenseId(Integer indvExpenseId) {
            this.indvExpenseId = indvExpenseId;
        }

        public Integer getIndvId() {
            return indvId;
        }

        public void setIndvId(Integer indvId) {
            this.indvId = indvId;
        }

        public String getExpenseType() {
            return expenseType;
        }

        public void setExpenseType(String expenseType) {
            this.expenseType = expenseType;
        }

        public String getExpenseFrequency() {
            return expenseFrequency;
        }

        public void setExpenseFrequency(String expenseFrequency) {
            this.expenseFrequency = expenseFrequency;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }
    }
