package com.ssp.dashboard.api.db.dao;


import com.ssp.dashboard.api.db.domain.Registration;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RegistrationDao extends JpaRepository<Registration, Integer> {

    List<Registration> findAll();
}