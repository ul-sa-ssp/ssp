package com.ssp.dashboard.api.db.dao;

import com.ssp.dashboard.api.db.domain.RegistrarIndividual;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RegistrarIndividualDao extends JpaRepository<RegistrarIndividual, Integer> {

    List<RegistrarIndividual> findAll();
}
