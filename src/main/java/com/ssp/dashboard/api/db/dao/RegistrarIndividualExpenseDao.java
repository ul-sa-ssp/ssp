package com.ssp.dashboard.api.db.dao;

import com.ssp.dashboard.api.db.domain.RegistrarIndividualExpense;
import com.ssp.dashboard.api.db.domain.RegistrarIndividualIncome;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RegistrarIndividualExpenseDao extends JpaRepository<RegistrarIndividualExpense, Integer> {

    List<RegistrarIndividualExpense> findAll();
}
