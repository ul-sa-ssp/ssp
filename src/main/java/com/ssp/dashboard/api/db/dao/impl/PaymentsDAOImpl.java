package com.ssp.dashboard.api.db.dao.impl;


import com.ssp.dashboard.api.db.dao.PaymentDAO;
import com.ssp.dashboard.api.db.domain.PaymentsEntity;
//import org.hibernate.Query;
//import org.hibernate.Session;
//import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class PaymentsDAOImpl implements PaymentDAO {
    private EntityManager entityManager;

    @Autowired
    public PaymentsDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<PaymentsEntity> getDetails(String scheme) {

        StringBuilder q = new StringBuilder();
        q.append( "select * FROM dc_payouts where scheme_name = :scheme ");

        Query nativeQuery = entityManager.createNativeQuery(q.toString(),PaymentsEntity.class);
        nativeQuery.setParameter("scheme",scheme);

        final List<PaymentsEntity> resultList = (List) nativeQuery.getResultList();

        return resultList;
    }

    @Override
    public List<PaymentsEntity> getAllPayments() {
        return null;
    }

    @Override
    public void makePayment(PaymentsEntity paymentsEntity) {

    }
}
