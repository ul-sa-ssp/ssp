package com.ssp.dashboard.api.db.dao;


import com.ssp.dashboard.service.MetricsVisitorService;

public interface VisitableEntity {
    public void accept(MetricsVisitorService metricsVisitorService);
}
