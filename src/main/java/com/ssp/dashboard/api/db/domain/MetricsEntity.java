package com.ssp.dashboard.api.db.domain;


import com.ssp.dashboard.api.db.dao.VisitableEntity;
import com.ssp.dashboard.service.MetricsVisitorService;

import java.util.Date;

public class MetricsEntity implements VisitableEntity {

    private String schemeName;
    private String metricsName;
    private long countOfTotalApplicants;
    private float totalAmountPaid;
    private Date dateOfPayment;

    public MetricsEntity(String schemeName, String metricsName, long countOfTotalApplicants, float totalAmountPaid) {
        this.schemeName = schemeName;
        this.metricsName = metricsName;
        this.countOfTotalApplicants = countOfTotalApplicants;
        this.totalAmountPaid = totalAmountPaid;
    }

    public long getCountOfTotalApplicants() {
        return countOfTotalApplicants;
    }

    public void setCountOfTotalApplicants(long countOfTotalApplicants) {
        this.countOfTotalApplicants = countOfTotalApplicants;
    }

    public float getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(float totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }

    public Date getDateOfPayment() {
        return dateOfPayment;
    }

    public void setDateOfPayment(Date dateOfPayment) {
        this.dateOfPayment = dateOfPayment;
    }

    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public String getMetricsName() {
        return metricsName;
    }

    public void setMetricsName(String metricsName) {
        this.metricsName = metricsName;
    }

    @Override
    public void accept(MetricsVisitorService metricsVisitorService) {
        metricsVisitorService.updateDetailsByEntity(this);
    }
}
