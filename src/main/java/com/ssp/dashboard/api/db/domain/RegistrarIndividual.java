package com.ssp.dashboard.api.db.domain;

import javax.persistence.*;

@Entity
@Table(name = "dc_indv")
public class RegistrarIndividual {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "indv_id")
    private Integer indvId;

    @Column(name = "app_id")
    private Integer appId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "birth_date")
    private String birthDate;

    @Column(name = "marital_status")
    private String maritalStatus;

    @Column(name = "relationship_to_applicant")
    private String relationshipToApplication;

    @Column(name = "household_status")
    private String householdStatus;

    @Column(name = "education_level")
    private String educationLevel;

    @Column(name = "earning_member")
    private String earningMember;

    public Integer getIndvId() {
        return indvId;
    }

    public void setIndvId(Integer indvId) {
        this.indvId = indvId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getRelationshipToApplication() {
        return relationshipToApplication;
    }

    public void setRelationshipToApplication(String relationshipToApplication) {
        this.relationshipToApplication = relationshipToApplication;
    }

    public String getHouseholdStatus() {
        return householdStatus;
    }

    public void setHouseholdStatus(String householdStatus) {
        this.householdStatus = householdStatus;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }

    public String getEarningMember() {
        return earningMember;
    }

    public void setEarningMember(String earningMember) {
        this.earningMember = earningMember;
    }
}
