package com.ssp.dashboard.api.db.dao;



import com.ssp.dashboard.api.db.domain.PaymentsEntity;

import java.util.List;

public interface PaymentDAO {

    public List<PaymentsEntity> getAllPayments();

    public void makePayment(PaymentsEntity paymentsEntity);

    public List<PaymentsEntity> getDetails(String scheme);

}
