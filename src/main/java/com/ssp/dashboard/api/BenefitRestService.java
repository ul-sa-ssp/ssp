package com.ssp.dashboard.api;

import com.ssp.dashboard.api.common.ApiResponse;
import com.ssp.dashboard.api.common.ApiResponseBuilder;
import com.ssp.dashboard.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("benefits")

public class BenefitRestService {
    @Autowired
    private BenefitsCalcService benefitsCalcService;

    @Autowired
    private WeightingService weightingService;

    @Autowired
    private BenefitsCalcFactoryService benefitsCalcFactoryService;

    @Autowired
    private WeightingFactoryService weightingFactoryService;

    @PostMapping("/benefitCalc/{type}/{location}")
    public ApiResponse createBenefitCalculation(@PathVariable String type, @PathVariable String location) {
        benefitsCalcService = benefitsCalcFactoryService.getInstance(type);
        weightingService = weightingFactoryService.getInstance(location);
        return ApiResponseBuilder.success().data(benefitsCalcService.calculateBenefitAmount(weightingService)).build();
    }

}
