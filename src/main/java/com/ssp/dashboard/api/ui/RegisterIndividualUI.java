package com.ssp.dashboard.api.ui;

public class RegisterIndividualUI {

    private String firstName;

    private String lastName;

    private String birthDate;

    private String maritalStatus;

    private String relationshipToApplication;

    private String householdStatus;

    private String educationLevel;

    private String earningMember;

    private String incomeType;

    private String incomeFrequency;

    private Double incomeAmount;

    private String expenseType;

    private Double expenseAmount;

    private String expenseFrequency;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getRelationshipToApplication() {
        return relationshipToApplication;
    }

    public void setRelationshipToApplication(String relationshipToApplication) {
        this.relationshipToApplication = relationshipToApplication;
    }

    public String getHouseholdStatus() {
        return householdStatus;
    }

    public void setHouseholdStatus(String householdStatus) {
        this.householdStatus = householdStatus;
    }

    public String getEducationLevel() {
        return educationLevel;
    }

    public void setEducationLevel(String educationLevel) {
        this.educationLevel = educationLevel;
    }

    public String getEarningMember() {
        return earningMember;
    }

    public void setEarningMember(String earningMember) {
        this.earningMember = earningMember;
    }

    public String getIncomeType() {
        return incomeType;
    }

    public void setIncomeType(String incomeType) {
        this.incomeType = incomeType;
    }

    public String getIncomeFrequency() {
        return incomeFrequency;
    }

    public void setIncomeFrequency(String incomeFrequency) {
        this.incomeFrequency = incomeFrequency;
    }

    public Double getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(Double incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public Double getExpenseAmount() {
        return expenseAmount;
    }

    public void setExpenseAmount(Double expenseAmount) {
        this.expenseAmount = expenseAmount;
    }

    public String getExpenseFrequency() {
        return expenseFrequency;
    }

    public void setExpenseFrequency(String expenseFrequency) {
        this.expenseFrequency = expenseFrequency;
    }
}
