package com.ssp.dashboard.api;

import com.ssp.dashboard.api.common.ApiResponse;
import com.ssp.dashboard.api.common.ApiResponseBuilder;
import com.ssp.dashboard.api.common.PaymentStatus;
import com.ssp.dashboard.api.ui.RegistrationUI;
import com.ssp.dashboard.service.ActivityInterceptorService;
import com.ssp.dashboard.service.PaymentDetailsService;
import com.ssp.dashboard.service.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("payment")
public class PaymentRestService {

    @Autowired
    OnlinePaymentDetailsServiceImpl onlinePaymentDetailsServiceImpl;

    @Autowired
    CashPaymentDetailsServiceImpl cashPaymentDetailsServiceImpl;

    @Autowired
    PaymentDetailsService paymentDetailsService;

    // Registering the interceptor for the notification service
    @Autowired
    private ActivityInterceptorService activityInterceptorService = new ActivityInterceptorServiceImpl();

    @PostMapping("pay/{type}")
    public ApiResponse createUserRegistration(@PathVariable String type, HttpServletRequest request) {

        String url = request.getRequestURI();
        activityInterceptorService.registerService(url);
        activityInterceptorService.dispatchService(url);

        if(type.equals("online"))
        switch (type){
            case "online":
                onlinePaymentDetailsServiceImpl.AccountNumber = 123123123;
                onlinePaymentDetailsServiceImpl.PaymentStatus = PaymentStatus.Pending;
                onlinePaymentDetailsServiceImpl.BillingAddress = "loran, Limerick,Ireland";
                paymentDetailsService = new OnlinePaymentDetailsAdapterServiceImpl(onlinePaymentDetailsServiceImpl);
                break;

            case "cash":
                cashPaymentDetailsServiceImpl.NearByPostOffice = "Limerick";
                cashPaymentDetailsServiceImpl.PaymentStatus = PaymentStatus.PendingForCollection;
                cashPaymentDetailsServiceImpl.BillingAddress = "loran, Limerick,Ireland";
                paymentDetailsService = new CashPaymentDetailsAdapterServiceImpl(cashPaymentDetailsServiceImpl);
                break;

        }
        return ApiResponseBuilder.success().data(paymentDetailsService.GetPaymentDetails()).build();
    }
}
