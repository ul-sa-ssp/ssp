package com.ssp.dashboard.api;

import com.ssp.dashboard.api.common.ApiResponse;
import com.ssp.dashboard.api.common.ApiResponseBuilder;
import com.ssp.dashboard.service.LookupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("lookup")
public class LookupRestService {

    @Autowired
    private LookupService lookupService;

    @GetMapping( "/registrar" )
    public ApiResponse getAllRegistrar()
    {

        return ApiResponseBuilder.success()
                .data( lookupService.getRegistrar() )
                .build();

    }

    @GetMapping( "/registrarIndividual" )
    public ApiResponse getAllRegistrarIndividual()
    {

        return ApiResponseBuilder.success()
                .data( lookupService.getRegistrarIndividual() )
                .build();

    }

    @GetMapping( "/registrarIndividualIncome" )
    public ApiResponse getAllRegistrarIndividualIncome()
    {

        return ApiResponseBuilder.success()
                .data( lookupService.getRegistrarIndividualIncome() )
                .build();

    }

    @GetMapping( "/registrarIndividualExpense" )
    public ApiResponse getAllRegistrarIndividualExpense()
    {

        return ApiResponseBuilder.success()
                .data( lookupService.getRegistrarIndividualExpense() )
                .build();

    }
}
