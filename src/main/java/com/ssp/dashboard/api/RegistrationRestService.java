package com.ssp.dashboard.api;

import com.ssp.dashboard.api.common.ApiResponse;
import com.ssp.dashboard.api.common.ApiResponseBuilder;
import com.ssp.dashboard.api.common.RegisterIndividualFactoryImpl;
import com.ssp.dashboard.api.ui.RegisterIndividualUI;
import com.ssp.dashboard.api.ui.RegistrationUI;
import com.ssp.dashboard.service.RegisterIndividualFactoryService;
import com.ssp.dashboard.service.RegisterIndividualService;
import com.ssp.dashboard.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("registration")
public class RegistrationRestService {

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private RegisterIndividualService registerIndividualService;

    @Autowired
    private RegisterIndividualFactoryService registerIndividualFactoryService;

    @PostMapping("/registerUser")
    public ApiResponse createUserRegistration(@RequestBody RegistrationUI registrationUI) {
        registrationService.registrationUser(registrationUI);
        return ApiResponseBuilder.success().build();
    }

    @PostMapping("/registerIndividual/{id}/{type}")
    public ApiResponse createRegistrarIndividual(@RequestBody RegisterIndividualUI registerIndividualUI, @PathVariable Integer id, @PathVariable String type) {
        registerIndividualService= registerIndividualFactoryService.getInstance(type);
        registerIndividualService.registrarIndividual(registerIndividualUI, id);
        return ApiResponseBuilder.success().build();
    }

}
