package com.ssp.dashboard.api.common;

import com.ssp.dashboard.service.RegisterIndividualService;
import com.ssp.dashboard.service.impl.RegisterIndividualExpenseServiceImpl;
import com.ssp.dashboard.service.impl.RegisterIndividualIncomeServiceImpl;
import com.ssp.dashboard.service.impl.RegisterIndividualServiceImpl;

public class RegisterIndividualFactoryImpl {

    public RegisterIndividualService getInstance(String type){
        if(type.equals("individual")){
            return new RegisterIndividualServiceImpl();
        }
        else if(type.equals("income")){
            return new RegisterIndividualIncomeServiceImpl();
        }
        else{
            return new RegisterIndividualExpenseServiceImpl();
        }
    }
}
