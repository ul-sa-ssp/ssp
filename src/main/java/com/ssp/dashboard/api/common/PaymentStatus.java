package com.ssp.dashboard.api.common;

public enum PaymentStatus {

    Pending,Success,Failed,PendingForCollection
}
