package com.ssp.dashboard.api;

import com.ssp.dashboard.api.common.ApiResponse;
import com.ssp.dashboard.api.common.ApiResponseBuilder;
import com.ssp.dashboard.api.ui.RegistrationUI;
import com.ssp.dashboard.service.ActivityInterceptorService;
import com.ssp.dashboard.service.SendNotificationService;
import com.ssp.dashboard.service.impl.ActivityInterceptorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("notification")
public class NotificationRestService {

    @Autowired
    private SendNotificationService sendNotificationService;

    // Registering the interceptor for the notification service
    @Autowired
    private ActivityInterceptorService activityInterceptorService = new ActivityInterceptorServiceImpl();

    @PostMapping("/notify")
    public ApiResponse SendNotification(HttpServletRequest request) {

        String url = request.getRequestURI();
        activityInterceptorService.registerService(url);

        sendNotificationService.SendNotify();
        return ApiResponseBuilder.success().build();
    }
}
