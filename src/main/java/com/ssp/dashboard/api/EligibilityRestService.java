package com.ssp.dashboard.api;

import com.ssp.dashboard.api.common.ApiResponse;
import com.ssp.dashboard.api.common.ApiResponseBuilder;
import com.ssp.dashboard.service.*;
import com.ssp.dashboard.service.impl.DetermineEligibilityCommandServiceImpl;
import com.ssp.dashboard.service.impl.EDServiceImpl;
import com.ssp.dashboard.service.impl.EDTaskRunnerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("eligibility")
public class EligibilityRestService {

    @Autowired
    private EDServiceImpl edServiceImpl;

    @Autowired
    private EDTaskRunnerImpl edTaskRunnerImpl;

    @PostMapping("/eligibleApps")
    public ApiResponse createEligibleApps() throws InterruptedException {

        List<String> fuelAppIDs = new ArrayList<>();
        List<String> covidAppIDs = new ArrayList<>();

        fuelAppIDs.add("23");
        fuelAppIDs.add("24");
        fuelAppIDs.add("25");

        covidAppIDs.add("32");
        covidAppIDs.add("33");
        covidAppIDs.add("34");

        CommandService fuelEDrun = new DetermineEligibilityCommandServiceImpl(fuelAppIDs, "Fuel", edServiceImpl);
        edTaskRunnerImpl.getInstance().addCommand(fuelEDrun);
        CommandService covidEDrun = new DetermineEligibilityCommandServiceImpl(covidAppIDs, "Covid", edServiceImpl);
        edTaskRunnerImpl.getInstance().addCommand(covidEDrun);
        edTaskRunnerImpl.getInstance().shutdown();
        return ApiResponseBuilder.success().build();
    }


}