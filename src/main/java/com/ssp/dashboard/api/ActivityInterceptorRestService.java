package com.ssp.dashboard.api;


import com.ssp.dashboard.api.common.ApiResponse;
import com.ssp.dashboard.api.common.ApiResponseBuilder;
import com.ssp.dashboard.service.ActivityInterceptorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/activity")
public class ActivityInterceptorRestService {

    private ActivityInterceptorService activityInterceptorService;

    @Autowired
    public ActivityInterceptorRestService(ActivityInterceptorService activityInterceptorService) {
        this.activityInterceptorService = activityInterceptorService;
    }

    @GetMapping("/activityList")
    public ApiResponse getActivity() {
        return ApiResponseBuilder.success().data( activityInterceptorService.getActivityList() ).build();
    }
}
