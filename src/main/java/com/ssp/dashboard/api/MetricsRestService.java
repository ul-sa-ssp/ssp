package com.ssp.dashboard.api;


import com.ssp.dashboard.api.common.ApiResponse;
import com.ssp.dashboard.api.common.ApiResponseBuilder;
import com.ssp.dashboard.api.db.domain.MetricsEntity;
import com.ssp.dashboard.service.ActivityInterceptorService;
import com.ssp.dashboard.service.MetricsVisitorService;
import com.ssp.dashboard.service.impl.ActivityInterceptorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MetricsRestService {

    private ActivityInterceptorService activityInterceptorService;
    private MetricsVisitorService metricsVisitorService;

    @Autowired
    public MetricsRestService(MetricsVisitorService metricsVisitorService) {
        this.metricsVisitorService = metricsVisitorService;
        this.activityInterceptorService = new ActivityInterceptorServiceImpl();
    }

    @GetMapping("/schememetrics")
    public ApiResponse findAll(HttpServletRequest request) {

        String url = request.getRequestURI();
        activityInterceptorService.registerService(url);
        activityInterceptorService.dispatchService(url);
       return ApiResponseBuilder.success().data( metricsVisitorService.findAll() ).build();

    }

}
