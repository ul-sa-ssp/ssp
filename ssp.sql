create table app_reg(app_id int AUTO_INCREMENT PRIMARY KEY, first_name varchar(255) NOT NULL, last_name varchar(255) NOT NULL, email varchar(255), phone_number varchar(13), birth_date varchar(15)); 
create table dc_indv(indv_id int AUTO_INCREMENT PRIMARY KEY, app_id int NOT NULL, first_name varchar(255) NOT NULL, last_name varchar(255) NOT NULL, birth_date varchar(15), marital_status varchar(255), relationship_to_applicant varchar(255), household_status varchar(255), education_level varchar(255), earning_member varchar(255), CONSTRAINT fk_indv FOREIGN KEY (app_id) REFERENCES app_reg(app_id) ON UPDATE CASCADE ON DELETE CASCADE); 
create table dc_income(income_type varchar(255), income_frequency varchar(255), amount decimal(10,2), indv_id int NOT NULL, CONSTRAINT fk_income FOREIGN KEY (indv_id) REFERENCES dc_indv(indv_id) ON UPDATE CASCADE ON DELETE CASCADE); 
create table dc_expense(expense_type varchar(255), expense_frequency varchar(255), amount decimal(10,2), indv_id int NOT NULL, CONSTRAINT fk_expense FOREIGN KEY (indv_id) REFERENCES dc_indv(indv_id) ON UPDATE CASCADE ON DELETE CASCADE); 
ALTER TABLE `ssp`.`dc_income` 
ADD COLUMN `indv_income_id` INT(11) NOT NULL AFTER `indv_id`,
ADD PRIMARY KEY (`indv_income_id`);
;
ALTER TABLE `ssp`.`dc_income` 
CHANGE COLUMN `indv_income_id` `indv_income_id` INT(11) NOT NULL AUTO_INCREMENT ;
ALTER TABLE `ssp`.`dc_expense` 
ADD COLUMN `indv_expense_id` VARCHAR(45) NOT NULL AFTER `indv_id`,
ADD PRIMARY KEY (`indv_expense_id`);
;
ALTER TABLE `ssp`.`dc_expense` 
CHANGE COLUMN `indv_expense_id` `indv_expense_id` INT(11) NOT NULL ;

ALTER TABLE `ssp`.`dc_expense` 
CHANGE COLUMN `indv_expense_id` `indv_expense_id` INT(11) NOT NULL AUTO_INCREMENT ;

create table dc_payouts(
payment_id int AUTO_INCREMENT PRIMARY KEY,
scheme_name varchar(255),
payment_timestamp datetime,
amount decimal(10,2),
indv_id int);